package edu.eci.cosw.lab02;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import edu.eci.cosw.lab02.entities.Post;

public class MainActivity extends AppCompatActivity {

    EditText message;

    ImageView image;

    AlertDialog alert;
    int PICK=123;
    int REQUEST_IMAGE_CAPTURE=1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        message=(EditText) findViewById(R.id.editText);
        image=(ImageView) findViewById(R.id.imageView);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.image)
                .setPositiveButton(R.string.file, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        System.out.println("Imagen");
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);

                        startActivityForResult(Intent.createChooser(intent,"Select Picture"),PICK);
                    }
                })
                .setNeutralButton(R.string.cam, new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("camara");
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        System.out.println("cancel");
                    }
                });

        alert= builder.create();
        findViewById(R.id.sendImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.show();
            }
        });
    }

    public void send(View view){
        boolean valid=validate();
        if(valid){
            Intent intent= new Intent(this,PostActivity.class);
            Bundle bundle= new Bundle();
            System.out.println(message.getText().toString()+" "+((String) image.getTag()));
            Post post= new Post(message.getText().toString(),(String) image.getTag());
            bundle.putSerializable("post",post);
            System.out.println(bundle.getSerializable("post"));
            intent.putExtras(bundle);
            System.out.println(intent.getExtras().getSerializable("post"));
            startActivity(intent);
        }
    }

    public boolean validate(){
        boolean valid=true;
        if(message.getText().length()==0 && ((String) image.getTag()).length()==0){
            message.setError("Please enter either a message or select an image");
            valid=false;
        }else if(message.getText().length()<=50){
            message.setError("The message should have a length longer than 50 characters");
            valid=false;
        }
        return valid;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            Uri selectedImageUri = data.getData();
            System.out.println(selectedImageUri);
            image.setImageURI(selectedImageUri);
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }else if(requestCode ==REQUEST_IMAGE_CAPTURE && resultCode==Activity.RESULT_OK){
            //Uri selectedImageUri = data.getData();
            //System.out.println(data.get);
        }
    }
}
