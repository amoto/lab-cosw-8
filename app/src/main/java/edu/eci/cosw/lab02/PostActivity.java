package edu.eci.cosw.lab02;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import edu.eci.cosw.lab02.entities.Post;

public class PostActivity extends AppCompatActivity {
    Post post;
    TextView text;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        text= (TextView) findViewById(R.id.PostMessage);
        img= (ImageView) findViewById(R.id.PostImage);
        Intent intent = getIntent();
        post= (Post) intent.getExtras().getSerializable("post");
        text.setText(post.getMessage());
        //img.setImageURI(Uri.parse(post.getImageUri()));
    }
}
