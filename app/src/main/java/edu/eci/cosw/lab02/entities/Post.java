package edu.eci.cosw.lab02.entities;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.io.Serializable;

/**
 * Created by 2105684 on 3/15/17.
 */
@Table
public class Post extends SugarRecord implements Serializable{

    private String message;

    private String imageUri;

    public Post(){}

    public Post(String message,String imageUri){
        setMessage(message);
        setImageUri(imageUri);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }
}
