package edu.eci.cosw.lab02;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.orm.SugarContext;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import edu.eci.cosw.lab02.entities.Post;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @BeforeClass
    public static void setUp(){
        SugarContext.init(InstrumentationRegistry.getTargetContext());
    }
    @AfterClass
    public static void finish(){
        SugarContext.terminate();
    }
    @Test
    public void saveEntity(){
        Post post = new Post("Message here", "image uri here");
        post.save();
    }
    @Test
    public void loadEntity(){
        Post post = Post.findById(Post.class, 1);
    }
    @Test
    public void updateEntity(){
        Post post = Post.findById(Post.class, 1);
        post.setMessage("updated message here"); // modify the values
        post.setImageUri("image uri updated");
        post.save(); // updates the previous entry with new values.
    }
    @Test
    public void deleteEntity(){
        Post post = Post.findById(Post.class, 1);
        post.delete();
    }
}
